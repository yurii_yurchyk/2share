<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('login', 30)->unique();
            $table->string('email', 191)->unique();
            $table->string('password', 191);
            $table->timestamp('created_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
