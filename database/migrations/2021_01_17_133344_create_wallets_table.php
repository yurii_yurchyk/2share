<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWalletsTable extends Migration
{
    public function up()
    {
        Schema::create('wallets', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->integer('user_id')->unique();
            $table->double('balance')->nullable()->default(0);
            $table->timestamp('created_at');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');;
        });
    }

    public function down()
    {
        Schema::dropIfExists('wallets');
    }
}
