<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->string('type', 30);
            $table->integer('user_id');
            $table->integer('wallet_id');
            $table->integer('deposit_id')->nullable();
            $table->double('amount')->nullable()->default(0);
            $table->timestamp('created_at');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('wallet_id')->references('id')->on('wallets')
                ->onDelete('cascade');
            $table->foreign('deposit_id')->references('id')->on('deposits')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
