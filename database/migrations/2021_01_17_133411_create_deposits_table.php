<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositsTable extends Migration
{
    public function up()
    {
        Schema::create('deposits', function (Blueprint $table) {
            $table->integerIncrements('id');
            $table->integer('user_id');
            $table->integer('wallet_id');
            $table->double('invested')->nullable()->default(0);
            $table->double('percent')->nullable()->default(0);
            $table->smallInteger('active')->nullable()->default(0);
            $table->smallInteger('duration')->nullable()->default(0);
            $table->smallInteger('accrue_times')->nullable()->default(0);
            $table->timestamp('created_at');

            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('wallet_id')->references('id')->on('wallets')
                ->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('deposits');
    }
}
