<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class WalletsTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'balance'    => 100,
                'created_at' => Carbon::now(),

                'user_id' => 1,
            ],
        ];

        DB::table('wallets')->insert($data);
    }
}
