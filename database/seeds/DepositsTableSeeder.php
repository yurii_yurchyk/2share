<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class DepositsTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'invested'   => 10,
                'percent'    => 20,
                'active'     => true,
                //                'duration' => , // todo
                'created_at' => Carbon::now(),

                'accrue_times' => 0,
                'user_id'      => 1,
                'wallet_id'    => 1,

            ],
        ];

        DB::table('deposits')->insert($data);
    }
}
