<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id'             => 1,
                'login'          => 'TestUser',
                'email'          => 'testuser@example.com',
                'password'       => bcrypt(11111111),
                'created_at'     => Carbon::now(),
            ],
        ];

        DB::table('users')->insert($data);
    }
}
