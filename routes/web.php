<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

$methods = ['index', 'create', 'store',];
Route::resource('deposits', 'DepositController')
    ->only($methods)
    ->names('deposits');

$methods = ['index',];
Route::resource('transactions', 'TransactionController')
    ->only($methods)
    ->names('transactions');

Route::get('/wallet/top-up', 'WalletController@createTopUp')->name('wallets.top-up.create');
Route::post('/wallet/top-up', 'WalletController@storeTopUp')->name('wallets.top-up.store');


