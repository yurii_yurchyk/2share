## To up project

1. `cp .env.example .env`
2. `docker-compose -f docker/docker-compose.yml up --build -d`
3. `docker-compose -f docker/docker-compose.yml exec php-fpm composer install`
4. `docker-compose -f docker/docker-compose.yml exec php-fpm php artisan migrate`
5. `docker-compose -f docker/docker-compose.yml exec php-fpm php artisan key:generate`
6. `docker-compose -f docker/docker-compose.yml exec node npm install && npm run dev`
7. Open in your browser http://127.0.0.1:8081/

