<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Observers;
use App\Models;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Models\Deposit::observe(Observers\DepositObserver::class);
        Models\Transaction::observe(Observers\TransactionObserver::class);
        Models\User::observe(Observers\UserObserver::class);
        Models\Wallet::observe(Observers\WalletObserver::class);
    }
}
