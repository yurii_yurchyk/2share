<?php

namespace App\Http\Controllers;

use App\Http\Requests\WalletTopUpRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class WalletController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function createTopUp()
    {
        return view('cabinet.wallets.top-up');
    }

    public function storeTopUp(WalletTopUpRequest $request)
    {
        $user                  = Auth::user();
        $user->wallet->balance += $request->top_up_amount;

        $transactionEnter = Transaction::make([
            'type'   => Transaction::TYPE_ENTER,
            'amount' => $request->top_up_amount,
        ]);
        $transactionEnter->wallet()->associate($user->wallet);
        $transactionEnter->user()->associate($user);

        DB::transaction(function () use ($user, $transactionEnter) {
            $user->wallet->save();
            $transactionEnter->save();
        });

        return redirect()->route('home');
    }

}
