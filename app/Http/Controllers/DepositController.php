<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\Http\Requests\DepositCreateRequest;
use App\Models\Deposit;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;

class DepositController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user     = Auth::user();
        $deposits = $user->deposits()->orderBy('created_at', 'desc')->get();

        return view('cabinet.deposits.index', compact('deposits'));
    }

    public function create()
    {
        return view('cabinet.deposits.create');
    }

    public function store(DepositCreateRequest $request)
    {
        $user                  = Auth::user();
        $user->wallet->balance -= $request->investing_amount;

        $deposit = Deposit::make([
            'invested' => $request->investing_amount,
            'percent'  => Deposit::DEFAULT_PERCENT,
            'active'   => true,
        ]);
        $deposit->user()->associate($user);
        $deposit->wallet()->associate($user->wallet);

        $transactionCreateDeposit = Transaction::make([
            'type'   => Transaction::TYPE_CREATE_DEPOSIT,
            'amount' => $request->investing_amount,
        ]);
        $transactionCreateDeposit->wallet()->associate($user->wallet);
        $transactionCreateDeposit->user()->associate($user);

        DB::transaction(function () use ($user, $deposit, $transactionCreateDeposit) {
            $user->wallet->save();
            $deposit->save();

            $transactionCreateDeposit->deposit()->associate($deposit)
                ->save();
        });

        return redirect()->route('deposits.index');
    }

}
