<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user         = Auth::user();
        $transactions = $user->transactions()->orderBy('created_at', 'desc')->get();

        return view('cabinet.transactions.index', compact('transactions'));
    }

}
