<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class DepositCreateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $user               = Auth::user();
        $balance            = $user->wallet->balance;
        $maxInvestingAmount = $balance < 100 ? $balance : 100;

        return [
            'investing_amount' => "required|integer|min:20|max:$maxInvestingAmount",
        ];
    }
}
