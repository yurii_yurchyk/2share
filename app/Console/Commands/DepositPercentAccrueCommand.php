<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\User;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class DepositPercentAccrueCommand extends Command
{
    protected $signature = 'deposit:accrue';

    protected $description = 'Add the user a percentage of the deposit';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        User::with('wallet')->chunk(100, function (Collection $users) {
            foreach ($users as $user) {
                $this->accrue($user);
            }
        });

        return 0;
    }

    private function accrue(User $user)
    {
        $activeDeposits = $user->deposits()->active()->get();
        foreach ($activeDeposits as $deposit) {
            $accrualAmount         = $deposit->calculateAccrualAmount();
            $user->wallet->balance += $accrualAmount;

            $transactionAccrual = Transaction::make([
                'type'   => Transaction::TYPE_ACCRUE,
                'amount' => $accrualAmount,
            ]);
            $transactionAccrual->user()->associate($user);
            $transactionAccrual->wallet()->associate($user->wallet);
            $transactionAccrual->deposit()->associate($deposit);

            $deposit->incrementAccrueTimes();
            $transactionCloseDeposit = null;
            if ($deposit->isNotActive()) {
                $transactionCloseDeposit = Transaction::make([
                    'type' => Transaction::TYPE_CLOSE_DEPOSIT,
                ]);
                $transactionCloseDeposit->user()->associate($user);
                $transactionCloseDeposit->wallet()->associate($user->wallet);
                $transactionCloseDeposit->deposit()->associate($deposit);
            }

            DB::transaction(function () use ($transactionAccrual, $user, $deposit, $transactionCloseDeposit) {
                $transactionAccrual->save();
                $user->wallet->save();
                $deposit->save();
                if ($transactionCloseDeposit) {
                    $transactionCloseDeposit->save();
                }
            });

        }

    }
}
