<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App\Models
 *
 * @property Wallet $wallet;
 * @property Deposit $deposit;
 */
class Transaction extends Model
{
    public $timestamps = false;
    public $dates      = ['created_at'];

    const TYPE_ENTER          = 'enter';
    const TYPE_CREATE_DEPOSIT = 'create_deposit';
    const TYPE_ACCRUE         = 'accrue';
    const TYPE_CLOSE_DEPOSIT  = 'close_deposit';

    protected $fillable = [
        'type',
        'amount',
        'created_at',
    ];

    protected $hidden = [];

    public static function scopeTypeAccrue($q)
    {
        return $q->where('type', static::TYPE_ACCRUE);
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class, 'wallet_id', 'id');
    }

    public function deposit()
    {
        return $this->belongsTo(Deposit::class);
    }
}
