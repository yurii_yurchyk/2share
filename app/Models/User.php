<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class Transaction
 * @package App\Models
 *
 * @property Wallet $wallet;
 * @property Deposit $deposits;
 */
class User extends Authenticatable
{
    use Notifiable;

    public $timestamps = false;
    public $dates      = ['created_at'];

    protected $fillable = [
        'login',
        'email',
        'password',
        'created_at',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function wallet()
    {
        return $this->hasOne(Wallet::class);
    }

    public function deposits()
    {
        return $this->hasMany(Deposit::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

}
