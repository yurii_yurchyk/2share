<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Deposit
 * @package App\Models
 *
 * @property User $user;
 * @property Wallet $wallet;
 * @property Transaction $transactions;
 */
class Deposit extends Model
{
    public $timestamps = false;
    public $dates      = ['created_at'];

    const MAX_ACCRUE_TIMES = 10;
    const DEFAULT_PERCENT  = 20;

    protected $fillable = [
        'invested',
        'percent',
        'active',
        'duration',
        'accrue_times',
        'created_at',
    ];

    protected $hidden = [];

    public static function scopeActive($q)
    {
        return $q->where('active', true);
    }

    public function isNotActive(): bool
    {
        return !$this->active;
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function wallet()
    {
        return $this->belongsTo(Wallet::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function accruedAmount()
    {
        $amount = $this->transactions()->typeAccrue()->sum('amount');

        return $amount;
    }

    public function incrementAccrueTimes()
    {
        $this->accrue_times++;

        if ($this->accrue_times == $this::MAX_ACCRUE_TIMES) {
            $this->active = false;
        }
    }

    public function calculateAccrualAmount()
    {
        return $this->invested * ($this->percent / 100);
    }

}
