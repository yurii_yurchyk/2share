<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App\Models
 *
 * @property User $user;
 * @property Transaction $transactions;
 */
class Wallet extends Model
{
    public $timestamps = false;
    public $dates      = ['created_at'];

    protected $fillable = [
        'balance',
        'created_at',
    ];

    protected $hidden = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

}
