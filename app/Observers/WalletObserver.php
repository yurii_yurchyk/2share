<?php

namespace App\Observers;

use App\Models\Wallet;
use Illuminate\Support\Carbon;

class WalletObserver
{
    public function creating(Wallet $wallet)
    {
        $wallet->created_at = Carbon::now();
    }

    public function created(Wallet $wallet)
    {
        //
    }

    public function updated(Wallet $wallet)
    {
        //
    }

    public function deleted(Wallet $wallet)
    {
        //
    }

    public function restored(Wallet $wallet)
    {
        //
    }

    public function forceDeleted(Wallet $wallet)
    {
        //
    }
}
