<?php

namespace App\Observers;

use App\Models\User;
use App\Models\Wallet;
use Illuminate\Support\Carbon;

class UserObserver
{
    public function creating(User $user)
    {
        $user->created_at = Carbon::now();
    }

    public function created(User $user)
    {
        $wallet = new Wallet();
        $wallet->user()->associate($user);
        $wallet->save();
    }

    public function updated(User $user)
    {
        //
    }

    public function deleted(User $user)
    {
        //
    }

    public function restored(User $user)
    {
        //
    }

    public function forceDeleted(User $user)
    {
        //
    }
}
