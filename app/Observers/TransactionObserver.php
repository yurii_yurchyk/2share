<?php

namespace App\Observers;

use App\Models\Transaction;
use Carbon\Carbon;

class TransactionObserver
{
    public function creating(Transaction $transaction)
    {
        $transaction->created_at = Carbon::now();

    }

    public function created(Transaction $transaction)
    {
        //
    }

    public function updated(Transaction $transaction)
    {
        //
    }

    public function deleted(Transaction $transaction)
    {
        //
    }

    public function restored(Transaction $transaction)
    {
        //
    }

    public function forceDeleted(Transaction $transaction)
    {
        //
    }
}
