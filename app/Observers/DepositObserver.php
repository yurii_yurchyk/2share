<?php

namespace App\Observers;

use App\Models\Deposit;
use Carbon\Carbon;

class DepositObserver
{
    public function creating(Deposit $deposit)
    {
        $deposit->created_at = Carbon::now();
    }

    public function created(Deposit $deposit)
    {
        //
    }

    public function updated(Deposit $deposit)
    {
        //

    }

    public function updating(Deposit $deposit)
    {
        //
    }

    public function deleted(Deposit $deposit)
    {
        //
    }

    public function restored(Deposit $deposit)
    {
        //
    }

    public function forceDeleted(Deposit $deposit)
    {
        //
    }
}
