@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <div class="card">
          <div class="card-header">Cabinet</div>

          <div class="card-body">
            @if (session('status'))
              <div class="alert alert-success" role="alert">
                {{ session('status') }}
              </div>
            @endif

            You are logged! <br>
              <?php /** @var \App\Models\User $user */ ?>
            Login: {{ $user->login }}<br>
            Email: {{ $user->email }}<br><br>
            Balance: {{ $user->wallet->balance }}<br>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
