@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">
        <form method="POST" action="{{ route('wallets.top-up.store') }}">
          @csrf

          <div class="form-group row">
            <label for="top_up_amount" class="col-md-4 col-form-label text-md-right">Top-up amount</label>

            <div class="col-md-6">
              <input id="top_up_amount" type="number"
                     class="form-control @error('top_up_amount') is-invalid @enderror" name="top_up_amount"
                     value="{{ old('top_up_amount') }}" required autocomplete="top_up_amount" autofocus>
            </div>
          </div>

          <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                Send
              </button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
@endsection
