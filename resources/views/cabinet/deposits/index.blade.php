@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Сумма вклада</th>
                        <th scope="col">Процент</th>
                        <th scope="col">Количество текущих начислений</th>
                        <th scope="col">Сумма начислений</th>
                        <th scope="col">Статус депозита</th>
                        <th scope="col">Дата</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($deposits as $deposit)
                        <tr>
                            <th scope="row">{{ $deposit->id }}</th>
                            <td>{{ $deposit->invested }}</td>
                            <td>{{ $deposit->percent }} %</td>
                            <td>{{ $deposit->accrue_times }}</td>
                            <td>{{ $deposit->accruedAmount() }}</td>
                            <td>{{ $deposit->active ? 'active' : 'closed'}}</td>
                            <td>{{ $deposit->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
