@extends('layouts.app')

@section('content')
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-8">

        <form method="POST" action="{{ route('deposits.store') }}">
          @csrf

          <div class="form-group row">
            <label for="investing_amount" class="col-md-4 col-form-label text-md-right">Investing amount (20 - 100)</label>

            <div class="col-md-6">
              <input id="investing_amount" type="number"
                     class="form-control @error('investing_amount') is-invalid @enderror" name="investing_amount"
                     value="{{ old('investing_amount') }}" required autocomplete="investing_amount" autofocus>
            </div>
          </div>

          <div class="form-group row mb-0">
            <div class="col-md-6 offset-md-4">
              <button type="submit" class="btn btn-primary">
                Send
              </button>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
@endsection
